WITH FilteredSubs(id, parent_id, name) 
AS (
	SELECT id, parent_id, name
	FROM dbo.subdivisions WHERE id =(SELECT subdivision_id FROM dbo.collaborators WHERE id = 710253)
	UNION ALL 
	SELECT t1.id, t1.parent_id, t1.name
	FROM dbo.subdivisions t1
	JOIN FilteredSubs t2 ON t1.parent_id = t2.id
),

SubLevels(id, parent_id, name, sub_level) 
AS (
	SELECT id, parent_id, name, 0 AS sub_level
	FROM dbo.subdivisions WHERE parent_id IS NULL
	UNION ALL 
	SELECT t1.id, t1.parent_id, t1.name, t2.sub_level + 1
	FROM dbo.subdivisions t1
	JOIN SubLevels t2 ON t1.parent_id = t2.id
),
FilteredCollaborators(id, name, subdivision_id, colls_count) AS (
	SELECT id, name, subdivision_id, [colls_count] = COUNT(*) OVER(PARTITION BY subdivision_id)
	FROM dbo.collaborators WHERE age < 40
)

SELECT *
FROM FilteredCollaborators AS fc
JOIN
(SELECT
	sl.name AS sub_name, sl.id AS sub_id, sl.sub_level
	FROM SubLevels as sl 
	JOIN FilteredSubs as fs
	ON sl.id = fs.id
	WHERE sl.id != 100059 AND sl.id != 100055
) tt
ON fc.subdivision_id = tt.sub_id
ORDER BY sub_level